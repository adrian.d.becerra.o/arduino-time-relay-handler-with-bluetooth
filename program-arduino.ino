/*--------------------------------------------------------------------------------------*
 *  Proyecto: Control de rele mediante horario manipualdo por bluetooth.                *
 *                                                                                      * 
 * T-ime                                                                                *
 * E-xact                                                                               *
 * R-elay                                                                               *
 * M-anipulation                                                                        *
 * A-wesome                                                                             *       
 *                                                                                      *
 *  Autores: Adrian y David Becerra.                                                    *
 *                                                                                      *
 *  Concepto: Este proyecto se enfoca en controlar de una corriente alterna mendiante   *
 *            un horario configurado por la comunicación bluetooth                      *
 *                                                                                      *
 *   Notas de los autores: -La aplicación desarrollada para la comunicación BT es por   *
 *                          el momento exclusiva para el SO android.                    *
 *                         -el control del horario se basa en un modulo de tiempo real  *
 *                         -El programa utiliza el protocolo I2C para el control de     *
 *                          una pantalla LCD de 2x16                                    *
 *                                                                                      *
 *--------------------------------------------------------------------------------------*/


#define DEBUG_ARRAY(a) {for (int index = 0; index < sizeof(a) / sizeof(a[0]); index++){Serial.print(a[index]); Serial.print('\t');} Serial.println();};
 
String str = "";            //
const char separator = ','; //  Estas variables se se utilizan para la separación de valores dentro de la recepción de datos
const int dataLength = 8;   //
int data[dataLength];       //


// Librerias:
#include <Wire.h> 
#include "RTClib.h"   // Control del tiempo
#include <LiquidCrystal_I2C.h> // Control de la pantalla LCD mendiante el protocolo I2C 

int n_dia;
int r_dia;
int n_mes;
int t_mes;
int n_anno;
int d_anno;
int t_siglo=6;
String s_dia;

int rele = 2; //salida del rele
String R;    //estado del rele

int contador; //este contador se encargar de llevar cuenta de ciclos para realizar acciones de manera periodica sin alterar el tiempo del ciclo
LiquidCrystal_I2C lcd(0x3F,16,2);
RTC_DS1307 RTC;

void setup () {
 Wire.begin(); // Inicia el puerto I2C
 RTC.begin(); // Inicia la comunicaci¢n con el RTC
// RTC.adjust(DateTime(__DATE__, __TIME__)); // Establece la fecha y hora (Comentar una vez establecida la hora)
 //el anterior se usa solo en la configuracion inicial
 Serial.begin(9600); // Establece la velocidad de datos del puerto serie
 lcd.init();
 lcd.backlight();
 lcd.clear();
 pinMode(rele,OUTPUT);
}

void loop(){

if (Serial.available())
   {
      str = Serial.readStringUntil('\n');             //
      for (int i = 0; i < dataLength ; i++)           //
      {                                               //   Separación de datos.
         int index = str.indexOf(separator);          //
         data[i] = str.substring(0, index).toInt();   //
         str = str.substring(index + 1);              //
      }
   }

  
 DateTime now = RTC.now(); // Obtiene la fecha y hora del RTC
   
 Serial.print(now.year(), DEC); // Año
 Serial.print('/');
 Serial.print(now.month(), DEC); // Mes
 Serial.print('/');
 Serial.print(now.day(), DEC); // Dia
 Serial.print(' ');
 Serial.print(now.hour(), DEC); // Horas
 Serial.print(':');
 Serial.print(now.minute(), DEC); // Minutos
 Serial.print(':');
 Serial.print(now.second(), DEC); // Segundos

 lcd.setCursor(12,1);

 n_anno=(now.year()-2000);
 d_anno=n_anno/4;
 n_dia=now.day();
 n_mes=now.month();

 switch (n_mes) {
    case 1:
      t_mes=0;
      break;
    case 2:
      t_mes=3;
      break;
    case 3:
      t_mes=3;
      break;
    case 4:
      t_mes=6;
      break;
    case 5:
      t_mes=1;
      break;
    case 6:
      t_mes=4;
      break;
    case 7:
      t_mes=6;
      break;
    case 8:
      t_mes=2;
      break;
    case 9:
      t_mes=5;
      break;
    case 10:
      t_mes=0;
      break;
    case 11:
      t_mes=3;
      break;
    case 12:
      t_mes=5;
      break;
    default: 
      t_mes=t_mes;
    break;
 }

 r_dia=n_dia+t_mes+n_anno+d_anno+t_siglo;
 r_dia = r_dia % 7;

 switch (r_dia) {
    case 1:

        Serial.println(" Lun");
        s_dia = "Lun";
      break;
      case 2:
       
       Serial.println(" Mar");
       s_dia = "Mar";
      break;
      case 3:
    
       Serial.println(" Mie");
       s_dia = "Mie";
      break;
      case 4:
      
       Serial.println(" Jue");
       s_dia = "Jue";
      break;
      case 5:

       Serial.println(" Vie");
       s_dia = "Vie";
      break;
      case 6:

       Serial.println(" Sab");
       s_dia = "Sab";
      break;
      case 0:
     
       Serial.println(" Dom");
       s_dia = "Dom";
      break;
      default: 
  
       Serial.println(" ---");
       s_dia = "---";
      break;
 }


 lcd.setCursor(12,0);
if
  ( now.hour()<= (((data[0])-1)) && (s_dia =="Lun" || s_dia == "Mar" || s_dia == "Mie" || s_dia == "Jue" || s_dia == "Vie") )
  { 
  digitalWrite(rele,LOW);
  Serial.print(" OFF");
  Serial.println();
  R="OFF";
  }
else 
    if
      ( now.hour()>= ((data[2])+1) && (s_dia =="Lun" || s_dia == "Mar" || s_dia == "Mie" || s_dia == "Jue" ||s_dia ==  "Vie") )
      { 
      digitalWrite(rele,LOW);
      Serial.print(" OFF");
      Serial.println();
      R="OFF";
      }
     else
         if
          ( now.hour()<=((data[4])-1)  && (s_dia == "Sab" || s_dia == "Dom") )
          { 
          digitalWrite(rele,LOW);
          Serial.print(" OFF");
          Serial.println();
          R="OFF";
          
          }
          else
              if
              ( now.hour()>= ((data[6])+1) && (s_dia == "Sab" || s_dia == "Dom") )
              { 
              digitalWrite(rele,LOW);
              Serial.print(" OFF");
              Serial.println("");
              R= "OFF";
              
              }
      else    
      {
      digitalWrite(rele,HIGH);
      Serial.print(" ON");
      Serial.println();
      R = "ON";
      }    
    Serial.println();Serial.println();Serial.println();
   contador = contador + 1;
   int D0 = (data[0]);
   int D1 = (data[1]);
   int D2 = (data[2]);
   int D3 = (data[3]);
   int D4 = (data[4]);
   int D5 = (data[5]);
   int D6 = (data[6]);
   int D7 = (data[7]);
   Serial.print("W-D:");Serial.print(D0);Serial.print(":");Serial.print(D1);Serial.print("to");Serial.print(D2);Serial.print(":");Serial.println(D3);
  if (contador > 10){
    lcd.setCursor(0,0);lcd.print("                 ");
    lcd.setCursor(0,1);lcd.print("                 ");
    lcd.setCursor(0,0);lcd.print("WD-->");
    lcd.setCursor(7,0);
    lcd.print(D0);
    //lcd.setCursor(7,0)
 //   ;lcd.print(":");
 //   lcd.setCursor(8,0);
 //   lcd.print(D1);
    lcd.setCursor(10,0);
    lcd.print("|");
     lcd.setCursor(13,0);
    lcd.print(D2);
    lcd.setCursor(13,0);
 //   lcd.print(":");
 //   lcd.setCursor(14,0);
  //  lcd.print(D3);
    lcd.setCursor(0,1);
    lcd.print("WE-->");
    lcd.setCursor(7,1);
    lcd.print(D4);
 //   lcd.setCursor(7,1);
   // lcd.print(":");
 //   lcd.setCursor(8,1);
  //  lcd.print(D5);
    lcd.setCursor(10,1);
    lcd.print("|");
    lcd.setCursor(13,1);
    lcd.print(D6);
 //   lcd.setCursor(13,1);
    //lcd.print(":");
  //  lcd.setCursor(14,1);
    //lcd.print(D7);
   }else{
     lcd.setCursor(0,0);
 lcd.print("D:");
 lcd.print(now.year(), DEC);
 lcd.print("/");
 lcd.print(now.month(), DEC);
 lcd.print("/");
 lcd.print(now.day(), DEC);
 lcd.print(" ");
 lcd.print(R);
 lcd.setCursor(0,1);
 lcd.print("T: ");
 lcd.print(now.hour(), DEC);
 lcd.print(":");
 lcd.print(now.minute(), DEC);
 lcd.print(":");
 lcd.print(now.second(), DEC);
 lcd.print(" ");
 lcd.print(s_dia);
   }
   if (contador > 20){
    contador = 0;
   }
  Serial.println(contador);
 delay(998); // La informaci¢n se actualiza cada 1 seg.
 lcd.setCursor(0,0);
 lcd.print("                ");
 lcd.setCursor(0,1);
 lcd.print("                ");
Serial.println(D7);
    
}